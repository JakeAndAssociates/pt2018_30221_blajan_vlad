package polinoame;

import java.util.*;

public class Polinom {

	public Map<Integer, Double> polinom = new HashMap<Integer, Double>();
	
	public Polinom()
	{
		
	}
	
	public Polinom(String str)
	{
		parsePolinom(str);
	}
	
	public void parsePolinom(String str)
	{
		boolean minusFlag = false;
		boolean coefFlag = false;
		
		double coef = 1;
		
		for(String val : str.split("x\\^|\\ | ")) 
		{
			if(val.equals("-"))
			{
				minusFlag = true;
				continue;
			}
			
			if(val.equals("+"))
				continue;
			
			if(coefFlag == false)
			{
				coef = Double.parseDouble(val);
				if(minusFlag)
				{
					coef= -coef;
					minusFlag = false;
				}
				coefFlag = true;
			}
			else
			{
				if(coef != 0.0)
					polinom.put(Integer.parseInt(val), coef);
				coefFlag = false;
			}
		}
		
	}
	
	public String printPolinom()
	{
		String rez = new String("");
		
		Iterator it = this.polinom.entrySet().iterator();
		
		while(it.hasNext())
		{
			Map.Entry<Integer, Double> pair = (Map.Entry<Integer, Double>)it.next();
			if(pair.getValue() == 0)
				continue;
			if(pair.getValue() > 0)
			{
				rez += "+";
				System.out.print("+");
			}
			System.out.print(pair.getValue() + "x^" + pair.getKey());
			rez += pair.getValue() + "x^" + pair.getKey();
		}
		
		if(rez.equals(""))
			rez += "0";
		
		System.out.println();
		return (rez);
	}
	
	public Polinom add(Polinom other)
	{
		Polinom rez = new Polinom();
		
		Iterator it = this.polinom.entrySet().iterator();
		
		while(it.hasNext())
		{
			Map.Entry<Integer, Double> pair = (Map.Entry<Integer, Double>)it.next();
			if(other.polinom.containsKey(pair.getKey()))
			{
				rez.polinom.put(pair.getKey(), pair.getValue() + other.polinom.get(pair.getKey()));
			}
			else
			{
				rez.polinom.put(pair.getKey(), pair.getValue());
			}
		}
		
		
		it = other.polinom.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry<Integer, Double> pair = (Map.Entry<Integer, Double>)it.next();
			if(!rez.polinom.containsKey(pair.getKey()))
			{
				rez.polinom.put(pair.getKey(), pair.getValue());
			}
		}
		return rez;
	}
	
	public Polinom dif(Polinom other)
	{
		Polinom rez = new Polinom();
		
		Iterator it = this.polinom.entrySet().iterator();
		
		while(it.hasNext())
		{
			Map.Entry<Integer, Double> pair = (Map.Entry<Integer, Double>)it.next();
			if(other.polinom.containsKey(pair.getKey()))
			{
				rez.polinom.put(pair.getKey(), pair.getValue() - other.polinom.get(pair.getKey()));
			}
			else
			{
				rez.polinom.put(pair.getKey(), pair.getValue());
			}
		}
		
		
		it = other.polinom.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry<Integer, Double> pair = (Map.Entry<Integer, Double>)it.next();
			if(!rez.polinom.containsKey(pair.getKey()))
			{
				rez.polinom.put(pair.getKey(), -pair.getValue());
			}
		}
		return rez;
	}
	
	public Polinom mul(Polinom other)
	{
		Polinom rez = new Polinom();
		
		Iterator firstIt = this.polinom.entrySet().iterator();

		while(firstIt.hasNext())
		{
			Map.Entry<Integer, Double> firstPair = (Map.Entry<Integer, Double>)firstIt.next();
			Iterator secondIt = other.polinom.entrySet().iterator();
			
			while(secondIt.hasNext())
			{
				Map.Entry<Integer, Double> secondPair = (Map.Entry<Integer, Double>)secondIt.next();
				if(rez.polinom.containsKey(firstPair.getKey() + secondPair.getKey()))
					rez.polinom.replace(firstPair.getKey() + secondPair.getKey(), rez.polinom.get(firstPair.getKey() + secondPair.getKey()) + firstPair.getValue() * secondPair.getValue());
				else
					rez.polinom.put(firstPair.getKey() + secondPair.getKey(), firstPair.getValue() * secondPair.getValue());
			}
		}
		return rez;
	}
	
	@SuppressWarnings("unchecked")
	public Polinom div(Polinom other)
	{
		Polinom rez = new Polinom("0x^0");
		
		Iterator firstIt = this.polinom.entrySet().iterator();
		Iterator secondIt = other.polinom.entrySet().iterator();
		
		Map.Entry<Integer, Double> firstPair = null;
		Map.Entry<Integer, Double> secondPair = null;
		
		if(!firstIt.hasNext())
			return rez;
		if(!secondIt.hasNext())
			return this;
		
		while(firstIt.hasNext())
			firstPair= (Map.Entry<Integer, Double>)firstIt.next();
		while(secondIt.hasNext())
			secondPair = (Map.Entry<Integer, Double>)secondIt.next();
		
		
		int deimpartit = firstPair.getKey();
		int impartitor = secondPair.getKey();
		
		if(deimpartit < impartitor)
			return rez;
		
		Polinom auxDem = new Polinom();
		auxDem.polinom.putAll(this.polinom);
		
		while(deimpartit >= impartitor)
		{
			double coef = firstPair.getValue() / secondPair.getValue();
			int grad = firstPair.getKey() - secondPair.getKey();
			
			rez.polinom.put(grad, coef)
;
			Polinom auxImp = new Polinom();
			auxImp.polinom.putAll(other.polinom);
			auxImp = auxImp.mul(new Polinom(coef + "x^" + grad));
			auxDem = auxDem.dif(auxImp);
			
			firstIt = auxDem.polinom.entrySet().iterator();
			
			while(firstIt.hasNext())
			{
				firstPair= (Map.Entry<Integer, Double>)firstIt.next();
				
				if(firstPair.getValue() == 0.0)
				{
					auxDem.polinom.remove(firstPair.getKey());
					firstIt = auxDem.polinom.entrySet().iterator();
				}
			}
			
			firstIt = auxDem.polinom.entrySet().iterator();
			
			if(!firstIt.hasNext())
				break;
			
			while(firstIt.hasNext())
				firstPair= (Map.Entry<Integer, Double>)firstIt.next();
			deimpartit = firstPair.getKey();
			System.out.println(firstPair.getValue());
		}
		
		return rez;
	}
	
	public Polinom deriv()
	{
		Polinom rez = new Polinom();

		Iterator it = this.polinom.entrySet().iterator();
		
		while(it.hasNext())
		{
			double coef;
			int grad;
			Map.Entry<Integer, Double> pair = (Map.Entry<Integer, Double>)it.next();
			
			coef = pair.getValue() * pair.getKey();
			grad = pair.getKey() - 1;
			
			rez.polinom.put(grad, coef);
		}
		return rez;
	}
	
	public Polinom integ()
	{
		Polinom rez = new Polinom();

		Iterator it = this.polinom.entrySet().iterator();
		
		while(it.hasNext())
		{
			double coef;
			int grad;
			Map.Entry<Integer, Double> pair = (Map.Entry<Integer, Double>)it.next();
			
			coef = pair.getValue() / (pair.getKey() + 1);
			grad = pair.getKey() + 1;
			
			rez.polinom.put(grad, coef);
		}
		return rez;
	}
}
