package polinoame;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GUI {
	
	private JFrame meniu = new JFrame("Meniu");
	private JPanel panel = new JPanel();
	private JLabel firstPolinLabel = new JLabel("primul polinom");
	private JLabel secondPolinLabel = new JLabel("al doilea polinom");
	private JLabel rezPolinLabel = new JLabel();
	private JTextField firstPolinText = new JTextField();
	private JTextField secondPolinText = new JTextField();
	
	public JButton add = new JButton("add");
	public JButton dif = new JButton("dif");
	public JButton mul = new JButton("mul");
	public JButton div = new JButton("div");
	public JButton deriv = new JButton("deriv");
	public JButton integ = new JButton("integ");
	
	public GUI()
	{
		meniu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		meniu.setSize(500, 300);
		
		panel.add(firstPolinLabel);
		panel.add(firstPolinText);
		panel.add(secondPolinLabel);
		panel.add(secondPolinText);
		panel.add(add);
		panel.add(dif);
		panel.add(mul);
		panel.add(div);
		panel.add(deriv);
		panel.add(integ);
		panel.add(rezPolinLabel);
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		meniu.setContentPane(panel);
		meniu.setVisible(true);
	}
	
	public void add()
	{
		Polinom firstPolin = new Polinom(firstPolinText.getText().toString());
		Polinom secondPolin = new Polinom(secondPolinText.getText().toString());
		
		Polinom rez = firstPolin.add(secondPolin);
		
		rezPolinLabel.setText("rezultat : " + rez.printPolinom());
	}
	
	public void dif()
	{
		Polinom firstPolin = new Polinom(firstPolinText.getText());
		Polinom secondPolin = new Polinom(secondPolinText.getText());
		
		Polinom rez = firstPolin.dif(secondPolin);
		
		rezPolinLabel.setText("rezultat : " + rez.printPolinom());
	}
	
	public void mul()
	{
		Polinom firstPolin = new Polinom(firstPolinText.getText());
		Polinom secondPolin = new Polinom(secondPolinText.getText());
		
		Polinom rez = firstPolin.mul(secondPolin);
		
		rezPolinLabel.setText("rezultat : " + rez.printPolinom());
	}
	
	public void div()
	{
		Polinom firstPolin = new Polinom(firstPolinText.getText());
		Polinom secondPolin = new Polinom(secondPolinText.getText());
		
		Polinom cat = firstPolin.div(secondPolin);
		Polinom rest = firstPolin.dif(secondPolin.mul(cat));
		
		rezPolinLabel.setText("cat : " + cat.printPolinom() + "  rest : " + rest.printPolinom());
	}
	
	public void deriv()
	{
		Polinom firstPolin = new Polinom(firstPolinText.getText());
		
		Polinom rez = firstPolin.deriv();
		
		rezPolinLabel.setText("rezultat : " + rez.printPolinom());
	}
	
	public void integ()
	{
		Polinom firstPolin = new Polinom(firstPolinText.getText());
		
		Polinom rez = firstPolin.integ();
		
		rezPolinLabel.setText("rezultat : " + rez.printPolinom());
	}
}
